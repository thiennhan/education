# Step to install

## 1. Pull source code
`git clone https://gitlab.com/thiennhan/education.git`

# If you do not want to use docker. You can skip step 2 + 3
* Step 2 + 3 is for build a apache webserver

## 2. Install docker-compose
Follow this instruction: https://docs.docker.com/compose/install/
## 3. Start docker container
`cd education`

`docker-compose up -d education`

## 4.Run sql
4.1: Run init sql

Go to folder mysql. Run script init.sql

4.2: Update site address

`UPDATE wp_options SET option_value='<YOUR_SITE_ADDRESS>' WHERE option_name='siteurl' OR option_name='home';`

Replace <YOUR_SITE_ADDRESS> with your site address and run that script

For ex : 

`UPDATE wp_options SET option_value='https://google.com' WHERE option_name='siteurl' OR option_name='home';`

## 5. Update database config

Open file `wordpress/wp-config.php`

Change constant DB_USER,DB_PASSWORD,DB_HOST to your database server parameters