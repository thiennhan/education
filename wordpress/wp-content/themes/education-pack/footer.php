<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #main-content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */

?>

</div><!-- #main-content -->
<footer id="colophon" class="site-footer">
	<?php education_pack_footer_layout(); ?>
</footer><!-- #colophon -->
</div><!-- wrapper-container -->

<?php wp_footer(); ?>

<?php do_action( 'education_pack_space_body' ); ?>
<div class="fb-customerchat" page_id="1880823875278588"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '303260590162099',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.12'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>
